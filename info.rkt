#lang info
(define collection "racktris")
(define deps '("base" "raart" "lux"))
(define pkg-desc "Racket falling tetromino game built with Lux")
(define version "0.0")
(define pkg-authors '("cwebber"))
(define racket-launcher-names '("racktris"))
(define racket-launcher-libraries '("racktris.rkt"))
